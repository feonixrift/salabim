Design discussion
--------

**Handling time**

Can encode 'speed of time/interaction' as a send-speed by wrapping send-message inside both user and server nodes.

Should have separate speeds of interaction for users and servers.

Longer term would like to draw these from a distribution, and maybe do a second order time effect to simulate days.
